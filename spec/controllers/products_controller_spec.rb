require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'show action' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
    let!(:no_related_products) { create_list(:product, 4) }

    before do
      get :show, params: { id: product.id }
    end

    it 'has the proper http status' do
      expect(response.status).to eq 200
    end

    it 'show page' do
      expect(response).to render_template :show
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'assigns @related_products' do
      expect(assigns(:related_products)).to match_array related_products
    end

    it "assigns data @related_products max 4" do
      expect(assigns(:related_products).count).to eq 4
    end

    context 'When there are 5 related products' do
      let!(:related_product_5) { create(:product, taxons: [taxon]) }

      it 'assigns data @related_products max 4' do
        expect(assigns(:related_products).count).to eq 4
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { taxonomy.root }
    let(:taxon_child1) { create(:taxon, taxonomy: taxonomy, parent: taxon) }
    let(:taxon_child2) { create(:taxon, taxonomy: taxonomy, parent: taxon) }
    let!(:product1) { create(:product, taxons: [taxon_child1]) }
    let!(:product2) { create(:product, taxons: [taxon_child2]) }

    before do
      get :show, params: { id: taxon_child1.id }
    end

    it "Success of response" do
      expect(response).to be_successful
    end

    it '200 Response is returned' do
      expect(response).to have_http_status(:success)
    end

    it 'renders template show' do
      expect(response).to render_template :show
    end

    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq taxon_child1
    end

    it 'assigns @taxonomies' do
      expect(assigns(:taxonomies)).to match_array Spree::Taxonomy.all
    end

    it 'assigns @products' do
      expect(assigns(:products)).to match_array product1
    end
  end
end

require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "Spree::Product", type: :model do
    let(:taxonomy) { create(:taxonomy, name: 'taxonomy') }
    let(:taxon) { create(:taxon, name: 'taxon', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:top_product) { create(:product, name: 'top_product', taxons: [taxon]) }
    let!(:related_product) { create(:product, name: 'related_product', taxons: [taxon]) }
    let!(:no_related_product) { create(:product, name: 'no_related_product', taxons: []) }

    describe "same_taxon(where.not)" do
      it "Are you acquiring related products" do
        expect(Spree::Product.same_taxon(top_product)).to match_array(related_product)
      end

      it "Is it possible to exclude the same item" do
        expect(Spree::Product.same_taxon(top_product)).not_to include(top_product)
      end

      it "Is it possible to exclude items that are not related products" do
        expect(Spree::Product.same_taxon(top_product)).not_to include(no_related_product)
      end
    end
  end
end

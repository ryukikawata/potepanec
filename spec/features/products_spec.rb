require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:taxonomy) { create(:taxonomy, name: 'taxonomy') }
  given(:child_taxon) { create(:taxon, name: 'child_taxon', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  given!(:top_product) { create(:product, name: 'top_product', price: 20.00, taxons: [child_taxon]) }
  given!(:related_product) { create(:product, name: 'related_product', price: 19.00, taxons: [child_taxon]) }
  given!(:no_related_product) { create(:product, name: 'no_related_product', price: 18.00, taxons: []) }

  background do
    visit potepan_product_path(top_product.id)
  end

  scenario "Whether the correct item is displayed on the products page" do
    expect(page).to have_selector '.page-title h2', text: top_product.name
    expect(page).to have_current_path(potepan_product_path(top_product.id))
    expect(page).to have_content top_product.name
    expect(page).to have_content top_product.display_price
  end

  scenario "Whether related products are correctly displayed on the products pag" do
    expect(page).to have_current_path(potepan_product_path(top_product.id))
    within('.productBox') do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content no_related_product.name
      expect(page).not_to have_content no_related_product.display_price
    end
  end

  scenario "Whether the links of related products are functioning properly" do
    click_on 'related_product'
    expect(page).to have_current_path(potepan_product_path(related_product.id))
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price
  end

  scenario "Click the button to return to the list page" do
    click_on "一覧ページへ戻る"
    expect(page).to have_current_path(potepan_category_path(Spree::Taxon.root.id))
    expect(page).to have_selector '.productBox h5', text: top_product.name
    expect(page).to have_selector '.productBox h3', text: top_product.display_price
  end
end

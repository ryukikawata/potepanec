require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy, name: "Category") }
  let!(:taxon1) { create(:taxon, taxonomy: taxonomy, name: "Clothing", parent: taxonomy.root) }
  let!(:taxon2) { create(:taxon, taxonomy: taxonomy, name: "Mugs", parent: taxonomy.root) }
  let!(:taxon_child) { create(:taxon, taxonomy: taxonomy, name: "Shirts", parent: taxon1) }
  let!(:product1) { create(:product, price: 20, taxons: [taxon_child], name: "T-shirts") }
  let!(:product2) { create(:product, price: 30, taxons: [taxon2], name: "Stein") }

  before do
    visit potepan_category_path(taxon_child.id)
  end

  scenario "Clicking a category displays the category product list" do
    click_link(taxon2.name)
    expect(page).to have_selector '.productCaption h5', text: product2.name
    expect(page).to have_selector '.productCaption h3', text: product2.display_price
    expect(page).not_to have_selector '.productCaption h5', text: product1.name
    expect(page).not_to have_selector '.productCaption h3', text: product1.display_price
  end

  scenario "Click an item from category, go to item detail page" do
    click_link(product1.name)
    expect(page).to have_current_path(potepan_product_path(product1.id))
    expect(page).to have_selector '.media-body h2', text: product1.name
    expect(page).to have_selector '.media-body h3', text: product1.display_price
    expect(page).not_to have_selector '.media-body h2', text: product2.name
    expect(page).not_to have_selector '.media-body h3', text: product2.display_price
  end
end

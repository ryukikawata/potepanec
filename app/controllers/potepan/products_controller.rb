class Potepan::ProductsController < ApplicationController
  MAX_NUM_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.
      includes_price_image.
      same_taxon(@product).
      limit(MAX_NUM_OF_RELATED_PRODUCTS)
  end
end
